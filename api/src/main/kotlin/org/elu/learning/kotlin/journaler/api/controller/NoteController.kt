package org.elu.learning.kotlin.journaler.api.controller

import org.elu.learning.kotlin.journaler.api.data.NoteDTO
import org.elu.learning.kotlin.journaler.api.service.NoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/notes")
class NoteController {
    @Autowired
    private lateinit var service: NoteService

    /**
     * Get list of notes
     */
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getNotes() = service.getNotes()

    /**
     * Insert note.
     * It consumes JSON, that is: request body Note.
     */
    @PostMapping(
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun insertNote(@RequestBody note: NoteDTO) = service.insertNote(note)

    /**
     * Remove note by Id.
     * We introduced path variable for Id to pass.
     */
    @DeleteMapping(
            value = ["/{id}"],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun deleteNote(@PathVariable(name = "id") id: String) = service.deleteNote(id)

    /**
     * Update item.
     * It consumes JSON, that is: request body Note.
     * As result it returns updated Note.
     */
    @PutMapping(
            value = ["/{id}"],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun updateNote(@PathVariable(name = "id") id: String, @RequestBody note: NoteDTO): NoteDTO = service.updateNote(note)

    @PostMapping(
            value = ["/by_title"],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun getNotesByTitle(@RequestBody payload: NoteFindByTitleRequest): Iterable<NoteDTO> =
            service.findByTitle(payload.title)
}
