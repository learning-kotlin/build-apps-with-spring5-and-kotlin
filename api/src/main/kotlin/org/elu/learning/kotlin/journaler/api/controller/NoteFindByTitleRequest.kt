package org.elu.learning.kotlin.journaler.api.controller

data class NoteFindByTitleRequest(var title: String = "")
