package org.elu.learning.kotlin.journaler.api.controller

import java.util.*

data class TodoLaterThanRequest(var date: Date = Date())
