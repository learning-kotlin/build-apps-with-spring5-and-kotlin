package org.elu.learning.kotlin.journaler.api.controller

import org.elu.learning.kotlin.journaler.api.security.User
import org.elu.learning.kotlin.journaler.api.security.UserDTO
import org.elu.learning.kotlin.journaler.api.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/users")
class UserController {
    @Autowired
    lateinit var service: UserService

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUsers() = service.getUsers()

    @PutMapping(
            value = ["/admin"],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun insertAdmin(@RequestBody user: UserDTO) = service.saveAdmin(user)

    @PutMapping(
            value = ["/member"],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun insertMember(@RequestBody user: UserDTO) = service.saveMember(user)

    @DeleteMapping(
            value = ["/{id}"],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun deleteUser(@PathVariable(name = "id") id: String) = service.deleteUser(id)

    @PostMapping(
            produces = [MediaType.APPLICATION_JSON_VALUE],
            consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun updateUser(@RequestBody user: User): User? = service.updateUser(user)
}
