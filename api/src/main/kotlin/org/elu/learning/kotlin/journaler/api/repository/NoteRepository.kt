package org.elu.learning.kotlin.journaler.api.repository

import org.elu.learning.kotlin.journaler.api.data.Note
import org.springframework.data.repository.CrudRepository

interface NoteRepository : CrudRepository<Note, String> {
    fun findByTitle(title: String): Iterable<Note>
}
