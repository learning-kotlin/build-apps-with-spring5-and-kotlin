package org.elu.learning.kotlin.journaler.api.repository

import org.elu.learning.kotlin.journaler.api.security.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, String> {
    fun findOneByEmail(email: String): User?
}
