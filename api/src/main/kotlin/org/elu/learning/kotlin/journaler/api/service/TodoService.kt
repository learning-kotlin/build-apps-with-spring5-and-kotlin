package org.elu.learning.kotlin.journaler.api.service

import org.elu.learning.kotlin.journaler.api.data.Todo
import org.elu.learning.kotlin.journaler.api.data.TodoDTO
import org.elu.learning.kotlin.journaler.api.repository.TodoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service("Todo service")
class TodoService {
    @Autowired
    lateinit var repository: TodoRepository

    fun getTodos(): Iterable<TodoDTO> = repository.findAll().map(::TodoDTO)

    fun insertTodo(todo: TodoDTO): TodoDTO = TodoDTO(
            repository.save(
                    Todo(
                            title = todo.title,
                            message = todo.message,
                            schedule = todo.schedule,
                            location = todo.location ?: ""
                    )
            )
    )

    fun deleteTodo(id: String) = repository.deleteById(id)

    fun updateTodo(todoDto: TodoDTO): TodoDTO {
        var todo = repository.findById(todoDto.id).get()
        todo.title = todoDto.title
        todo.message = todoDto.message
        todo.schedule = todoDto.schedule
        todo.location = todoDto.location ?: ""
        todo.modified = Date()
        todo = repository.save(todo)
        return TodoDTO(todo)
    }

    fun getScheduledLaterThan(date: Date): Iterable<TodoDTO> {
        return repository.findScheduledLaterThan(date.time).map(::TodoDTO)
    }
}
